# Random Number Engines
```cpp
/** calculating the value of pi by "throwing stones", i.e.
 *  taking two random numbers and checking if they are inside
 *  the unit disk.
 **/

#include <iostream>
#include <random>

int main() {
    // set number of random values,
    // higher for increased precision
    const int number_of_throws = 50000000;
    int good_throws = 0;

    std::default_random_engine e;
    e.seed(42);

    //get max value of random machine
    const int MAX = e.max();

    std::cout << "e.min(): " << e.min() << std::endl;
    std::cout << "e.max(): " << e.max() << std::endl;

    //set precision of output to 10 digits
    std::cout.precision(10);


    for (int i = 0; i < number_of_throws; ++i) {

        //assign two random numbers to a and b
        double a { static_cast<double>(e()) };
        double b { static_cast<double>(e()) };

        // divide a and b by the random machine max value in order
        // to get values in the range from 0.0 to 1.0
        a /= MAX;
        b /= MAX;

        // check if the distance to the origin is less than 1
        // (this is the unit disk), increase the counter
        // for every value on the disk
        if (a * a + b * b <= 1)
            good_throws++;
    }

    // multiply the result by 4 in order to get a
    // whole unit disk and correct approximation of pi
    std::cout << static_cast<double>(good_throws)  /
    static_cast<double>(number_of_throws) * 4.0 << std::endl;

    return 0;
}
```
