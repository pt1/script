# Changes in C++14, C++17, C++20

## C++ concepts


```cpp
#include <type_traits>
// Require that T be an integral type.
template<typename T> concept C = std::is_integral_v<T>;

template<C T> void f(T) { }
void g ()
{
  f (1); // OK
  f (1.2); // error: use of function with unsatisfied constraints
}
```

## Constrained auto
```cpp
int fn1 ();
double fn2 ();

void h ()
{
  C auto x1 = fn1 (); // OK
  C auto x2 = fn2 (); // error: deduced initializer does not
                      // satisfy placeholder constraints
}
```

## constinit keyword

```cpp
constexpr int fn1 () { return 42; }
int fn2 () { return -1; }
constinit int i1 = fn1 (); // OK
constinit int i2 = fn2 (); // error: constinit variable does
                           // not have a constant initializer
```

## Designated initializers

```cpp
struct A { int x; int y; int z; };
A a{.y = 2, .x = 1}; // error; designator order does
                     // not match declaration order
A b{.x = 1, .z = 2}; // ok, b.y initialized to 0


struct A {
  string str;
  int n = 42;
  int m = -1;
};
A{.m=21}  // Initializes str with {}, which calls the
          // default constructor
          // then initializes n with = 42
          // then initializes m with = 21
```

## More on initializers

```cpp
#include <string>
#include <array>
struct S {
    int x;
    struct Foo {
        int i;
        int j;
        int a[3];
    } b;
};

union U {
    int a;
    const char* b;
};

int main()
{
    S s1 = { 1, { 2, 3, {4, 5, 6} } };
    S s2 = { 1, 2, 3, 4, 5, 6}; // same, but with brace elision
    S s3{1, {2, 3, {4, 5, 6} } }; // same, using direct-list-
                                  // initialization syntax
    S s4{1, 2, 3, 4, 5, 6}; // error in C++11: brace-elision
                            // only allowed with equals sign
                            // okay in C++14

    int ar[] = {1,2,3}; // ar is int[3]
    int ab[] (1, 2, 3); // (C++20) ab is int[3]
    // char cr[3] = {'a', 'b', 'c', 'd'}; // too many initializer
                                          // clauses
    char cr[3] = {'a'}; // array initialized as {'a', '\0', '\0'}

    int ar2d1[2][2] = {{1, 2}, {3, 4}}; // fully-braced 2D array:
                                        // {1, 2}
                                        // {3, 4}
    int ar2d2[2][2] = {1, 2, 3, 4}; // brace elision: {1, 2}
                                    //                {3, 4}
    int ar2d3[2][2] = {{1}, {2}};   // only first column: {1, 0}
                                    //                    {2, 0}

    std::array<int, 3> std_ar2{ {1,2,3} }; // std::array is an
                                           // aggregate
    std::array<int, 3> std_ar1 = {1, 2, 3}; // brace-elision okay

    int ai[] = { 1, 2.0 }; // narrowing conversion from double
                           // to int:
                           // error in C++11, okay in C++03

    std::string ars[] =
        {std::string("one"), // copy-initialization
         "two", // conversion, then copy-initialization
         {'t', 'h', 'r', 'e', 'e'} // list-initialization
        };

    U u1 = {1}; // OK, first member of the union
//    U u2 = { 0, "asdf" }; // error: too many
                            // initializers for union
//    U u3 = { "asdf" }; // error: invalid conversion to int

}

// aggregate
struct base1 { int b1, b2 = 42; };
// non-aggregate
struct base2 {
  base2() : b3(42) {}
  int b3;
};
// aggregate in C++17
struct derived : base1, base2 { int d; };
derived d1{ {1, 2}, { }, 4}; // d1.b1 = 1, d1.b2 = 2,  
                             // d1.b3 = 42, d1.d = 4
derived d2{ {    }, { }, 4}; // d2.b1 = 0, d2.b2 = 42,
                             // d2.b3 = 42, d2.d = 4
```
