# Contribute to PT1 script

If you'd like to contribute, please copy the `post-commit` hook into your git hooks, like so:
```bash
$ cp git-hooks/post-commit .git/hooks
```

## Open ToDos:
- Split script into one file per chapter
- Move script to folder, put contribute.md in README.md

## Info
1. The script is written in *markdown* in `README.md`
2. There is some further reading/more in-depth explanations in the `cheatsheets` folder
3. Images are done with extra html in the markdown
   - Add Image
   ```html
   <img src="graphics/pipeline.png" style="zoom:100%"/>
   ```
4. General formatting and other things like colored text
   - See [GitLab Markdown page](https://docs.gitlab.com/ee/user/markdown.html)
4. The Latex format is [Katex](https://github.com/KaTeX/KaTeX)
5. Create an issue for ToDo's

## Compile the Markdown
Compile using pandoc, this will also create the automatic table of contents (TOC). Currently this is done as part of the workflow in the `post-commit` hook.
```bash
#!/bin/sh
pandoc README.md -s --toc --highlight-style tango --metadata title="Programming Techniques for Scientific Simulations I" --katex -A footer.html -o script.html
git add script.html
git commit -nm "Autocommit: Update script.html"
git pull
git push
git checkout script
git checkout master -- script.html
git checkout master -- assets/
git checkout master -- cheatsheets/
git commit -nm "Autocommit: Update script and assets"
git pull
git push
git checkout master
exit
```

## Convert files in sources
```bash
pandoc --highlight-style tango -s -o ../cheatsheets/modern_cpp_bonus.pdf modern_cpp_bonus.md
```

## Setup in Repo

### Creating a Branch for Script
Create an empty branch and then only push the script, assets and cheatsheets.
This is already done and only left here for future reference:
```bash
$ git checkout --orphan script
$ git rm -rf .
$ git commit --allow-empty -m "Empty commit"
$ git push --set-upstream origin script
```

### Setup in Lecture Repo

`$ git submodule add git@gitlab.ethz.ch:pt1/script.git`

Change .gitmodules to include `branch = script`

`$ git submodule update --remote`

### For students in Lecture
The lecture script is provided as a git [submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules). In order to pull that
repository you need to initialize it with the following command:
```bash
$ git submodule update --init --remote
```
In order to check and get updates from the script repo, use the following:
```bash
$ git submodule update --remote
```

## Useful Links

- CMake: [repository with examples and explanations](https://github.com/ttroy50/cmake-examples)
- CPU Caches [Wikipedia](https://en.wikipedia.org/wiki/CPU_cache)
